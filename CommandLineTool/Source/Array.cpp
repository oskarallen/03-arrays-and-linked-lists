//
//  Array.cpp
//  CommandLineTool
//
//  Created by Michael Skween on 06/10/2017.
//  Copyright (c) 2017 Tom Mitchell. All rights reserved.
//

#include "Array.h"

Array::Array()
{
    arraySize = 0;
    floatPointer = nullptr;
    //floatPointer = new dynamicArray[0];
}

Array::~Array()
{
    delete[] floatPointer;
}

void Array:: add (float itemValue)
{
    if (arraySize == 0)
    {
        arraySize++;
        floatPointer = new float[arraySize];
        *floatPointer = itemValue;
    }
    
    else
    {
        arraySize++;
        float *tempPointer = new float[arraySize]; //create new array with the temp pointer pointing to it
        for (int i = 0; i < arraySize - 1; i ++)
        {
            tempPointer[i] = floatPointer[i]; //copy element of array from old array to new array
        }
        tempPointer[arraySize - 1] = itemValue;
        delete[] floatPointer;
        floatPointer = tempPointer;
    }
    
}

float Array:: get(int index) const
{
    if (index < arraySize || index > 0)
    {
        return floatPointer[index];
    }
    else
    {
        return 0;
    }
}

int Array:: size()
{
    return arraySize;
}

bool testArray()
{
    Array array;
    const float testArray[] = {0.f, 1.f, 2.f, 3.f, 4.f, 5.f};
    const int testArraySize = sizeof (testArray) / sizeof (testArray[0]);
    
    if (array.size() != 0)
    {
        std::cout << "size is incorrect\n";
        return false;
    }
    
    for (int i = 0; i < testArraySize; i++)
    {
        array.add (testArray[i]);
        
        if (array.size() != i + 1)
        {
            std::cout << "size is incorrect\n";
            return false;
        }
        
        if (array.get (i) != testArray[i])
        {
            std::cout << "value at index "<< i << " recalled incorrectly\n" ;
            return false;
        }
    }
    
    //    //removing first
    //    array.remove (0);
    //    if (array.size() != testArraySize - 1)
    //    {
    //        std::cout << "with size after removing item\n";
    //        return false;
    //    }
    //
    //    for (int i = 0; i < array.size(); i++)
    //    {
    //        if (array.get(i) != testArray[i+1])
    //        {
    //            std::cout << "problems removing items\n";
    //            return false;
    //        }
    //    }
    //
    //    //removing last
    //    array.remove (array.size() - 1);
    //    if (array.size() != testArraySize - 2)
    //    {
    //        std::cout << "with size after removing item\n";
    //        return false;
    //    }
    //
    //    for (int i = 0; i < array.size(); i++)
    //    {
    //        if (array.get(i) != testArray[i + 1])
    //        {
    //            std::cout << "problems removing items\n";
    //            return false;
    //        }
    //    }
    //
    //    //remove second item
    //    array.remove (1);
    //    if (array.size() != testArraySize - 3)
    //    {
    //        std::cout << "with size after removing item\n";
    //        return false;
    //    }
    //
    //    if (array.get (0) != testArray[1])
    //    {
    //        std::cout << "problems removing items\n";
    //        return false;
    //    }
    //
    //    if (array.get (1) != testArray[3])
    //    {
    //        std::cout << "problems removing items\n";
    //        return false;
    //    }
    //    
    //    if (array.get (2) != testArray[4])
    //    {
    //        std::cout << "problems removing items\n";
    //        return false;
    //    }
    
    return true;
}
