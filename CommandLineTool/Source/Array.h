//
//  Array.h
//  CommandLineTool
//
//  Created by Michael Skween on 06/10/2017.
//  Copyright (c) 2017 Tom Mitchell. All rights reserved.
//

#ifndef __CommandLineTool__Array__
#define __CommandLineTool__Array__

#include <stdio.h>
#include <iostream>

class Array
{
public:
    Array();
    ~Array();
    void add (float itemValue);
    float get (int index) const;
    int size();
    bool testArray();

private:
    int arraySize;
    float* floatPointer;
};

#endif /* defined(__CommandLineTool__Array__) */
