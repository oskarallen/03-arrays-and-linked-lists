//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>
#include "Array.h"

bool testArray(); //function to test Array class

int main (int argc, const char* argv[])
{
    
    if (testArray() == true)
    {
        std::cout << "all Array tests passed\n";
    }
    else
    {
        std::cout << "Array tests failed\n";
    }
    
    return 0;
}